# coding: utf-8

"""
    Alfresco Content Services REST API

    **Core API**  Provides access to the core features of Alfresco Content Services.   # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from alfresco_core_api_client.configuration import Configuration


class CompositeConditionDefinition(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'inverted': 'bool',
        'boolean_mode': 'str',
        'composite_conditions': 'list[CompositeConditionDefinition]',
        'simple_conditions': 'list[SimpleConditionDefinition]'
    }

    attribute_map = {
        'inverted': 'inverted',
        'boolean_mode': 'booleanMode',
        'composite_conditions': 'compositeConditions',
        'simple_conditions': 'simpleConditions'
    }

    def __init__(self, inverted=False, boolean_mode='and', composite_conditions=None, simple_conditions=None, _configuration=None):  # noqa: E501
        """CompositeConditionDefinition - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._inverted = None
        self._boolean_mode = None
        self._composite_conditions = None
        self._simple_conditions = None
        self.discriminator = None

        if inverted is not None:
            self.inverted = inverted
        if boolean_mode is not None:
            self.boolean_mode = boolean_mode
        if composite_conditions is not None:
            self.composite_conditions = composite_conditions
        if simple_conditions is not None:
            self.simple_conditions = simple_conditions

    @property
    def inverted(self):
        """Gets the inverted of this CompositeConditionDefinition.  # noqa: E501

        Whether to invert the logic for this condition (if true then \"not\" is applied to the whole condition)  # noqa: E501

        :return: The inverted of this CompositeConditionDefinition.  # noqa: E501
        :rtype: bool
        """
        return self._inverted

    @inverted.setter
    def inverted(self, inverted):
        """Sets the inverted of this CompositeConditionDefinition.

        Whether to invert the logic for this condition (if true then \"not\" is applied to the whole condition)  # noqa: E501

        :param inverted: The inverted of this CompositeConditionDefinition.  # noqa: E501
        :type: bool
        """

        self._inverted = inverted

    @property
    def boolean_mode(self):
        """Gets the boolean_mode of this CompositeConditionDefinition.  # noqa: E501

        How to combine the clauses of this condition (\"and\" or \"or\")  # noqa: E501

        :return: The boolean_mode of this CompositeConditionDefinition.  # noqa: E501
        :rtype: str
        """
        return self._boolean_mode

    @boolean_mode.setter
    def boolean_mode(self, boolean_mode):
        """Sets the boolean_mode of this CompositeConditionDefinition.

        How to combine the clauses of this condition (\"and\" or \"or\")  # noqa: E501

        :param boolean_mode: The boolean_mode of this CompositeConditionDefinition.  # noqa: E501
        :type: str
        """
        allowed_values = ["and", "or"]  # noqa: E501
        if (self._configuration.client_side_validation and
                boolean_mode not in allowed_values):
            raise ValueError(
                "Invalid value for `boolean_mode` ({0}), must be one of {1}"  # noqa: E501
                .format(boolean_mode, allowed_values)
            )

        self._boolean_mode = boolean_mode

    @property
    def composite_conditions(self):
        """Gets the composite_conditions of this CompositeConditionDefinition.  # noqa: E501

        Nested list of composite clauses in this condition  # noqa: E501

        :return: The composite_conditions of this CompositeConditionDefinition.  # noqa: E501
        :rtype: list[CompositeConditionDefinition]
        """
        return self._composite_conditions

    @composite_conditions.setter
    def composite_conditions(self, composite_conditions):
        """Sets the composite_conditions of this CompositeConditionDefinition.

        Nested list of composite clauses in this condition  # noqa: E501

        :param composite_conditions: The composite_conditions of this CompositeConditionDefinition.  # noqa: E501
        :type: list[CompositeConditionDefinition]
        """

        self._composite_conditions = composite_conditions

    @property
    def simple_conditions(self):
        """Gets the simple_conditions of this CompositeConditionDefinition.  # noqa: E501

        Nested list of simple (per field) conditions.  # noqa: E501

        :return: The simple_conditions of this CompositeConditionDefinition.  # noqa: E501
        :rtype: list[SimpleConditionDefinition]
        """
        return self._simple_conditions

    @simple_conditions.setter
    def simple_conditions(self, simple_conditions):
        """Sets the simple_conditions of this CompositeConditionDefinition.

        Nested list of simple (per field) conditions.  # noqa: E501

        :param simple_conditions: The simple_conditions of this CompositeConditionDefinition.  # noqa: E501
        :type: list[SimpleConditionDefinition]
        """

        self._simple_conditions = simple_conditions

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(CompositeConditionDefinition, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, CompositeConditionDefinition):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, CompositeConditionDefinition):
            return True

        return self.to_dict() != other.to_dict()
