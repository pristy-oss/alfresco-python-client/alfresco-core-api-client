# coding: utf-8

"""
    Alfresco Content Services REST API

    **Core API**  Provides access to the core features of Alfresco Content Services.   # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from alfresco_core_api_client.configuration import Configuration


class Pagination(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'count': 'int',
        'has_more_items': 'bool',
        'total_items': 'int',
        'skip_count': 'int',
        'max_items': 'int'
    }

    attribute_map = {
        'count': 'count',
        'has_more_items': 'hasMoreItems',
        'total_items': 'totalItems',
        'skip_count': 'skipCount',
        'max_items': 'maxItems'
    }

    def __init__(self, count=None, has_more_items=None, total_items=None, skip_count=None, max_items=None, _configuration=None):  # noqa: E501
        """Pagination - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._count = None
        self._has_more_items = None
        self._total_items = None
        self._skip_count = None
        self._max_items = None
        self.discriminator = None

        if count is not None:
            self.count = count
        if has_more_items is not None:
            self.has_more_items = has_more_items
        if total_items is not None:
            self.total_items = total_items
        if skip_count is not None:
            self.skip_count = skip_count
        if max_items is not None:
            self.max_items = max_items

    @property
    def count(self):
        """Gets the count of this Pagination.  # noqa: E501

        The number of objects in the entries array.   # noqa: E501

        :return: The count of this Pagination.  # noqa: E501
        :rtype: int
        """
        return self._count

    @count.setter
    def count(self, count):
        """Sets the count of this Pagination.

        The number of objects in the entries array.   # noqa: E501

        :param count: The count of this Pagination.  # noqa: E501
        :type: int
        """

        self._count = count

    @property
    def has_more_items(self):
        """Gets the has_more_items of this Pagination.  # noqa: E501

        A boolean value which is **true** if there are more entities in the collection beyond those in this response. A true value means a request with a larger value for the **skipCount** or the **maxItems** parameter will return more entities.   # noqa: E501

        :return: The has_more_items of this Pagination.  # noqa: E501
        :rtype: bool
        """
        return self._has_more_items

    @has_more_items.setter
    def has_more_items(self, has_more_items):
        """Sets the has_more_items of this Pagination.

        A boolean value which is **true** if there are more entities in the collection beyond those in this response. A true value means a request with a larger value for the **skipCount** or the **maxItems** parameter will return more entities.   # noqa: E501

        :param has_more_items: The has_more_items of this Pagination.  # noqa: E501
        :type: bool
        """

        self._has_more_items = has_more_items

    @property
    def total_items(self):
        """Gets the total_items of this Pagination.  # noqa: E501

        An integer describing the total number of entities in the collection. The API might not be able to determine this value, in which case this property will not be present.   # noqa: E501

        :return: The total_items of this Pagination.  # noqa: E501
        :rtype: int
        """
        return self._total_items

    @total_items.setter
    def total_items(self, total_items):
        """Sets the total_items of this Pagination.

        An integer describing the total number of entities in the collection. The API might not be able to determine this value, in which case this property will not be present.   # noqa: E501

        :param total_items: The total_items of this Pagination.  # noqa: E501
        :type: int
        """

        self._total_items = total_items

    @property
    def skip_count(self):
        """Gets the skip_count of this Pagination.  # noqa: E501

        An integer describing how many entities exist in the collection before those included in this list. If there was no **skipCount** parameter then the default value is 0.   # noqa: E501

        :return: The skip_count of this Pagination.  # noqa: E501
        :rtype: int
        """
        return self._skip_count

    @skip_count.setter
    def skip_count(self, skip_count):
        """Sets the skip_count of this Pagination.

        An integer describing how many entities exist in the collection before those included in this list. If there was no **skipCount** parameter then the default value is 0.   # noqa: E501

        :param skip_count: The skip_count of this Pagination.  # noqa: E501
        :type: int
        """

        self._skip_count = skip_count

    @property
    def max_items(self):
        """Gets the max_items of this Pagination.  # noqa: E501

        The value of the **maxItems** parameter used to generate this list. If there was no **maxItems** parameter then the default value is 100.   # noqa: E501

        :return: The max_items of this Pagination.  # noqa: E501
        :rtype: int
        """
        return self._max_items

    @max_items.setter
    def max_items(self, max_items):
        """Sets the max_items of this Pagination.

        The value of the **maxItems** parameter used to generate this list. If there was no **maxItems** parameter then the default value is 100.   # noqa: E501

        :param max_items: The max_items of this Pagination.  # noqa: E501
        :type: int
        """

        self._max_items = max_items

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(Pagination, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, Pagination):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, Pagination):
            return True

        return self.to_dict() != other.to_dict()
