# coding: utf-8

"""
    Alfresco Content Services REST API

    **Core API**  Provides access to the core features of Alfresco Content Services.   # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


import pprint
import re  # noqa: F401

import six

from alfresco_core_api_client.configuration import Configuration


class AuditApp(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """

    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'id': 'str',
        'name': 'str',
        'is_enabled': 'bool',
        'max_entry_id': 'int',
        'min_entry_id': 'int'
    }

    attribute_map = {
        'id': 'id',
        'name': 'name',
        'is_enabled': 'isEnabled',
        'max_entry_id': 'maxEntryId',
        'min_entry_id': 'minEntryId'
    }

    def __init__(self, id=None, name=None, is_enabled=True, max_entry_id=None, min_entry_id=None, _configuration=None):  # noqa: E501
        """AuditApp - a model defined in Swagger"""  # noqa: E501
        if _configuration is None:
            _configuration = Configuration()
        self._configuration = _configuration

        self._id = None
        self._name = None
        self._is_enabled = None
        self._max_entry_id = None
        self._min_entry_id = None
        self.discriminator = None

        self.id = id
        if name is not None:
            self.name = name
        if is_enabled is not None:
            self.is_enabled = is_enabled
        if max_entry_id is not None:
            self.max_entry_id = max_entry_id
        if min_entry_id is not None:
            self.min_entry_id = min_entry_id

    @property
    def id(self):
        """Gets the id of this AuditApp.  # noqa: E501


        :return: The id of this AuditApp.  # noqa: E501
        :rtype: str
        """
        return self._id

    @id.setter
    def id(self, id):
        """Sets the id of this AuditApp.


        :param id: The id of this AuditApp.  # noqa: E501
        :type: str
        """
        if self._configuration.client_side_validation and id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def name(self):
        """Gets the name of this AuditApp.  # noqa: E501


        :return: The name of this AuditApp.  # noqa: E501
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """Sets the name of this AuditApp.


        :param name: The name of this AuditApp.  # noqa: E501
        :type: str
        """

        self._name = name

    @property
    def is_enabled(self):
        """Gets the is_enabled of this AuditApp.  # noqa: E501


        :return: The is_enabled of this AuditApp.  # noqa: E501
        :rtype: bool
        """
        return self._is_enabled

    @is_enabled.setter
    def is_enabled(self, is_enabled):
        """Sets the is_enabled of this AuditApp.


        :param is_enabled: The is_enabled of this AuditApp.  # noqa: E501
        :type: bool
        """

        self._is_enabled = is_enabled

    @property
    def max_entry_id(self):
        """Gets the max_entry_id of this AuditApp.  # noqa: E501


        :return: The max_entry_id of this AuditApp.  # noqa: E501
        :rtype: int
        """
        return self._max_entry_id

    @max_entry_id.setter
    def max_entry_id(self, max_entry_id):
        """Sets the max_entry_id of this AuditApp.


        :param max_entry_id: The max_entry_id of this AuditApp.  # noqa: E501
        :type: int
        """

        self._max_entry_id = max_entry_id

    @property
    def min_entry_id(self):
        """Gets the min_entry_id of this AuditApp.  # noqa: E501


        :return: The min_entry_id of this AuditApp.  # noqa: E501
        :rtype: int
        """
        return self._min_entry_id

    @min_entry_id.setter
    def min_entry_id(self, min_entry_id):
        """Sets the min_entry_id of this AuditApp.


        :param min_entry_id: The min_entry_id of this AuditApp.  # noqa: E501
        :type: int
        """

        self._min_entry_id = min_entry_id

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(AuditApp, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, AuditApp):
            return False

        return self.to_dict() == other.to_dict()

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        if not isinstance(other, AuditApp):
            return True

        return self.to_dict() != other.to_dict()
