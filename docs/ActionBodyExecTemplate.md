# ActionBodyExecTemplate

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action_definition_id** | **str** |  | 
**params** | **object** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


