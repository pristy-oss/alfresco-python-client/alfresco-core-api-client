# Category

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | The identifier for the category. | 
**name** | **str** | The name of the category.  This must be unique within the parent category.  | 
**parent_id** | **str** | The id of the parent category (or -root- if this is a top level category). | [optional] 
**has_children** | **bool** | True if the category has at least one child category. | [optional] 
**count** | **float** | The number of nodes that are assigned to this category. | [optional] 
**path** | **str** | The path to this category. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


