# RestoreArchivedContentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restore_priority** | **str** | Restore from archive priority (Standard/High - to be mapped to Storage Provider specific values in Cloud Connectors) | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


