# ArchiveContentRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**archive_params** | **dict(str, str)** | Optional map (String-String) of archive request properties for given content.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


