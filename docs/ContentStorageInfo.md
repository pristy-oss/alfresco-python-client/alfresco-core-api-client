# ContentStorageInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** | Content type property identifier (e.g. cm:content). Inside this object only colon (&#39;:&#39;) delimiter for namespace-prefix will be used.  | 
**storage_properties** | **dict(str, str)** | A map (String-String) of storage properties for given content.  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


