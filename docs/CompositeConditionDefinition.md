# CompositeConditionDefinition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**inverted** | **bool** | Whether to invert the logic for this condition (if true then \&quot;not\&quot; is applied to the whole condition) | [optional] [default to False]
**boolean_mode** | **str** | How to combine the clauses of this condition (\&quot;and\&quot; or \&quot;or\&quot;) | [optional] [default to 'and']
**composite_conditions** | [**list[CompositeConditionDefinition]**](CompositeConditionDefinition.md) | Nested list of composite clauses in this condition | [optional] 
**simple_conditions** | [**list[SimpleConditionDefinition]**](SimpleConditionDefinition.md) | Nested list of simple (per field) conditions. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


