# ModelProperty

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**title** | **str** | the human-readable title | [optional] 
**description** | **str** | the human-readable description | [optional] 
**default_value** | **str** | the default value | [optional] 
**data_type** | **str** | the name of the property type (e.g. d:text) | [optional] 
**is_multi_valued** | **bool** | define if the property is multi-valued | [optional] 
**is_mandatory** | **bool** | define if the property is mandatory | [optional] 
**is_mandatory_enforced** | **bool** | define if the presence of mandatory properties is enforced | [optional] 
**is_protected** | **bool** | define if the property is system maintained | [optional] 
**constraints** | [**list[Constraint]**](Constraint.md) | list of constraints defined for the property | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


