# Definition

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**properties** | [**list[ModelProperty]**](ModelProperty.md) | List of property definitions effective for this node as the result of combining the type with all aspects. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


