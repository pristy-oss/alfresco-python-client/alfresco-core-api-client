# Constraint

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | 
**type** | **str** | the type of the constraint | [optional] 
**title** | **str** | the human-readable constraint title | [optional] 
**description** | **str** | the human-readable constraint description | [optional] 
**parameters** | **dict(str, object)** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


