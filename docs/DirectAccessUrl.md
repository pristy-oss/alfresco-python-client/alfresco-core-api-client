# DirectAccessUrl

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**content_url** | **str** | The direct access URL of a binary content | 
**attachment** | **bool** | Flag to control the download method, **true** for attachment URL, **false** for embedded URL | [optional] 
**expires_at** | **datetime** | The direct access URL would become invalid when the expiry date is reached | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


