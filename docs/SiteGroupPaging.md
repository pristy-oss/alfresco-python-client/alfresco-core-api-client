# SiteGroupPaging

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**list** | [**SiteGroupPagingList**](SiteGroupPagingList.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


