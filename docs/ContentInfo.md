# ContentInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**mime_type** | **str** |  | 
**mime_type_name** | **str** |  | [optional] 
**size_in_bytes** | **int** |  | [optional] 
**encoding** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


