# coding: utf-8

"""
    Alfresco Content Services REST API

    **Core API**  Provides access to the core features of Alfresco Content Services.   # noqa: E501

    OpenAPI spec version: 1
    
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

import unittest

import swagger_client
from swagger_client.models.archive_content_request import ArchiveContentRequest  # noqa: E501
from swagger_client.rest import ApiException


class TestArchiveContentRequest(unittest.TestCase):
    """ArchiveContentRequest unit test stubs"""

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testArchiveContentRequest(self):
        """Test ArchiveContentRequest"""
        # FIXME: construct object with mandatory attributes with example values
        # model = swagger_client.models.archive_content_request.ArchiveContentRequest()  # noqa: E501
        pass


if __name__ == '__main__':
    unittest.main()
